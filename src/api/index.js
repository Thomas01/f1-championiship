import axios from 'axios';

const baseUrl = 'https://ergast.com/api/f1';

// GET SEASONS
export const getSeasons = async () => {
    try{
        const {data :{MRData :{StandingsTable :{StandingsLists}}}} = await axios.get(`${baseUrl}/driverStandings/1.json?limit=18&offset=55`);
        const actualData = StandingsLists.map(data =>({
          win : data.DriverStandings[0].wins,
          season : data.season,
          firstName : data.DriverStandings[0].Driver.givenName,
          lastName : data.DriverStandings[0].Driver.familyName,
          position : data.DriverStandings[0].Driver.permanentNumber,
          points : data.DriverStandings[0].points,
          id : data.DriverStandings[0].Driver.driverId
      }));
        return actualData
    }catch(error){
        console.log(error)
    }
}

//GET SEASON DETAILS
export const getSeasonsDetails = async (season) => {
  try{
      const {data :{MRData :{RaceTable :{Races}}}} = await axios.get(`${baseUrl}/${season}/5/results.json`);
      const raceData = Races.map(data => data.Results); 
      const final = raceData.map(e => e.map(list =>({
        position : list.position,
        number : list.number,
        firstName : list.Driver.familyName,
        lastName : list.Driver.givenName,
        contructor: list.Constructor.constructorId,
        laps : list.laps,
        grid : list.grid,
       //time: list.Time.time,
        status : list.status,
        points : list.points
      })))
      return final
  }catch(error){
      console.log(error)
  }

 
}
  