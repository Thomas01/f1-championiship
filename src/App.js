import './App.css';
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import React from 'react';
import NavBar from './components/NavBar';
import HomeScreen from './components/screens/HomeScreen';

const App = () => {
  return (
    <BrowserRouter>
        <div>
          <NavBar /> 
          <Routes>
          <Route path="/" element={<HomeScreen />} />
          </Routes>
        </div>
    </BrowserRouter> 
  ) 
}

export default App;
