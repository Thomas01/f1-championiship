import './SeasonDetails'
import React from 'react';

const SeasonDetails = ({ year, tableDetails, backButton }) => {

  return (
    <div className='wrapper'>
    <div className='head-wrapper'>
    <h1>List of results for { year }</h1> 
    <button className='back' onClick={()=>backButton()}> Back </button>
    <table>
      <thead>
        <tr>
          <th>Position</th>
          <th>Number</th>
          <th>Names</th>
          <th>Contructor</th>
          <th>Lap</th>
          <th>Grid</th>
          <th>Points</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        { 
            tableDetails && tableDetails.map(r =>r.map((e, i) => (
                <tr key = {i}>
                 <td>{e.position}</td>
                 <td>{e.number}</td>
                 <td>{`${e.firstName} ${e.lastName}`}</td>
                 <td>{e.contructor}</td>
                 <td>{e.laps}</td>
                 <td>{e.grid}</td>
                 <td>{e.points}</td>
                 <td>{e.status}</td>
               </tr>
            )))
        }
      </tbody>
      </table>
    </div>
    <div>
  </div>   
</div>
  );
}

export default SeasonDetails;
