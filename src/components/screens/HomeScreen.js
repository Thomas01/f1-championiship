import React, {useState, useEffect} from 'react';
import { getSeasons, getSeasonsDetails } from '../../api';
import Home from '../Home';
import SeasonDetails from '../SeasonDeatail/SeasonDetails';

const HomeScreen = () => {

const [seasons, setSeasons] = useState([])
const [selectedSeason, setelectedSeason] = useState([])
const [tableDetails, setTableDetails] = useState([])
const [year, setYear] = useState()

// LOAD SEASONS
useEffect(()=>{
const getInfo = async() => {
setSeasons(await getSeasons());
}
getInfo();
}, [])

const handleSelectSeason = async (selectedSeason) => {
const years = await getSeasonsDetails(selectedSeason.season)
setTableDetails(years )
setelectedSeason( selectedSeason );
setYear(selectedSeason.season)
}

const backButton = () => {
setelectedSeason([])
//window.location.reload()
}

let display
if (!selectedSeason.season) {
display = <Home 
seasons = {seasons}
handleSelectSeason={handleSelectSeason}
/>
} else {
display = <SeasonDetails
selectedSeason = { selectedSeason.season }
tableDetails = { tableDetails } 
year = {year}
backButton = {backButton}
/>

}
  return (
    <div className='wrapper'>
        <div className='head-wrapper'>
        {display}
        </div>
    </div>
  );
}

export default HomeScreen;
