import './NavBar.css';
import React from 'react';
import { Link } from "react-router-dom";
import logo from '../assets/logo.png'

const NavBar = () => {
  return (
    <div className = 'navBar'> 
         <div className='navBar-center'>
            <div className='nav-logo'>
              <img src={logo} alt='Logo' />
            </div>
            <div className='links'>
                <Link to ="/"> Home</Link>
                <Link to ="#">Search</Link>
                <Link to ="#">Contact</Link>
                <Link to ="#">Login</Link>
            </div>
         </div>
    </div>
  );
}

export default NavBar;
