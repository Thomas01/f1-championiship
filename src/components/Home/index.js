import './Home.css';
import React from 'react';

const Home = ({seasons, handleSelectSeason}) => {
  return (
      <div className='wrapper' >
          <div className='head-wrapper'>
          <h1>F1 World ChampionShip Winners</h1>
        <table>
            <thead>
              <tr>
                <th>Year</th>
                <th>Position</th>
                <th>Wins</th>
                <th>Points</th>
                <th>Names</th>
                <th>View Details</th>
              </tr>
            </thead>
            <tbody>
              { seasons && seasons.map((e, i) => (
                   <tr key = {i}>
                   <td>{e.season}</td>
                   <td>{e.position}</td>
                   <td>{e.win}</td>
                   <td>{e.points}</td>
                   <td>{`${e.firstName} ${e.lastName}`}</td>
                   <td className='buttonCenter'>
                   <button
                    className ="selectButton"
                    onClick={()=>handleSelectSeason(e, i)}
                   >
                     View Details
                   </button>
                   </td>
                 </tr>
              ))
              }
            </tbody>
            </table>
          </div>
          <div>
        </div>   
      </div>
  );
}

export default Home;
